import _ from "lodash"; // used for pagination

export function paginate(items, pageNumber, pageSize) {
  const startIndex = (pageNumber - 1) * pageSize; // calc start index of items in current page
  return _(items) // take all items from current page by converting it to a lodash wrapper : returns lodash Object
    .slice(startIndex) // slice array from start index
    .take(pageSize) // take item for current page
    .value(); // return new regular array
}
