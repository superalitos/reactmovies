import React, { Component } from "react";
import Table from "./common/table";
import Favourite from "./common/favourite";

class MoviesTable extends Component {
  columns = [
    { path: "title", label: "Title" },
    { path: "genre.name", label: "Genre" },
    { path: "numberInStock", label: "Stock" },
    { path: "dailyRentalRate", label: "rate" },
    {
      key: "like",
      content: movie => (
        <Favourite
          liked={movie.liked}
          onClick={() => this.props.onLike(movie)}
        />
      )
    },
    {
      key: "delete",
      content: movie => (
        <button
          onClick={() => this.propsonDelete(movie)}
          className="btn btn-secondary btn-sm"
        >
          DELETE
        </button>
      )
    }
  ];
  render() {
    const { movies, onSort, sortColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        data={movies}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default MoviesTable;
